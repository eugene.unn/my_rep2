import styles from '../styles/Home.module.scss'
import SideBarModule from "./sidebar/sidebar.module";

export default function MainLayout({children}) {
    return (
        <main className={styles.main}>
            <SideBarModule/>
            {children}
        </main>
    )
}
