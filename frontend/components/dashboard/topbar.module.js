import style from "./TopBar.module.scss";

export default function TopBarModule() {
    return (
        <div className={style.topbar}>
            <h3>Российская Федерация</h3>
        </div>
    )
}
