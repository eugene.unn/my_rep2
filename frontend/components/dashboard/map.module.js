import mapStyle from "./Map.module.scss";
import {MapContainer, Marker, Polygon, TileLayer, useMap} from "react-leaflet";
import 'leaflet/dist/leaflet.css'
import {useContext, useEffect, useState} from "react";
import L from "leaflet";
import ModalModule from "./modal.module";
import Image from "next/image";
import Link from "next/link";
import style from "./Modal.module.scss"
import {useMyContext} from "../../context/state";

function Hexagon({id, h3, color}) {
    const map = useMap()
    const [isShowModal, setIsShowModal] = useState(false)
    const [isShowHistoryModal, setShowHistoryModal] = useState(false)


    let [pathOptions, setPathOptions] = useState({
        weight: 0.3,
        opacity: 1,
        fillOpacity: 0.5,
        fillColor: color,
        color: "#3388ff", // stroke color
    })

    let [isClicked, setIsClicked] = useState(false)

    return (
        <Polygon
            key={id}
            positions={h3.coordinates}
            pathOptions={pathOptions}
            eventHandlers={{
                click: (event) => {
                    map.flyTo([55.115305, 37.309144], 16)
                    setTimeout(() => {
                        setIsClicked(true)
                        setPathOptions({
                            weight: 0,
                            opacity: 0,
                            fillOpacity: 0,
                            fillColor: "#fff",
                            color: "#fff", // stroke color
                        })
                    }, 5000)
                }
            }}
        >
            {isClicked ?
                <Marker
                    position={[55.11573683395504, 37.305677463720954]}
                    icon={redIcon}
                    eventHandlers={
                        {
                            click: (event) => {
                                setIsShowModal(true)
                            }
                        }
                    }
                /> : null
            }
            {isClicked ?
                <Marker
                    position={[55.11591289539586, 37.308929995637676]}
                    icon={redIcon}
                    eventHandlers={
                        {
                            click: (event) => {
                                setIsShowModal(true)
                            }
                        }
                    }
                /> : null
            }
            {isClicked ?
                <Marker
                    position={[55.116084776109254, 37.31195466024345]}
                    icon={redIcon}
                    eventHandlers={
                        {
                            click: (event) => {
                                setIsShowModal(true)
                            }
                        }
                    }
                /> : null
            }
            {isClicked ?
                <Marker
                    position={[55.11645308943331, 37.315386903767724]}
                    icon={blueIcon}
                    eventHandlers={
                        {
                            click: (event) => {
                                setIsShowModal(true)
                            }
                        }
                    }
                /> : null
            }
            {isShowModal ? <ModalModule style={{position: 'absolut'}}>
                <div className={style.modal__header}>
                    <h3>Пролет 2-3</h3>
                </div>
                <div className={style.modal__body}>
                    <div style={{margin: '0 0 30px'}}>
                        <div style={{fontSize: '18px'}}>Ответственные</div>
                        <div style={{fontSize: '24px'}}>Энергосетевая компания</div>
                    </div>

                    <div style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row'
                    }}>
                        <div style={{width: '49%', textAlign: 'left'}}>
                            <div style={{color: '#006EFF', fontWeight: 'bold', fontSize: 24}}>Опора №1</div>
                            <div style={{fontSize: 20, padding: '16px 0 0'}}>34.4024618 53.9713152</div>
                        </div>
                        <div style={{width: '49%', textAlign: 'left'}}>
                            <div style={{color: '#006EFF', fontWeight: 'bold', fontSize: 24}}>Опора №2</div>
                            <div style={{fontSize: 20, padding: '16px 0 0'}}>34.4024618 53.9713152</div>
                        </div>
                    </div>
                    <div className={style.small} style={{margin: '40px 0 0'}}>
                        Класс напряжения
                        <strong>110</strong>
                    </div>
                    <div className={style.two_column} style={{margin: '30px 0 0'}}>
                        <div style={{width: '49%', textAlign: 'left'}}>
                            <div className={style.small} style={{margin: '0 0 16px'}}>
                                Тип опоры
                            </div>
                            <div className={style.medium_text}>
                                1,2 УБ110-5
                            </div>
                        </div>
                        <div style={{width: '49%', textAlign: 'left'}}>
                            <div className={style.small} style={{margin: '0 0 16px'}}>
                                Тип опоры
                            </div>
                            <div className={style.medium_text}>
                                1,2 УБ110-5
                            </div>
                        </div>
                    </div>
                    <div className={style.small} style={{margin: '40px 0 0'}}>
                        Дата <strong className={style.medium_text}>03.12.2021</strong>
                    </div>
                    <div className={style.two_column} style={{margin: '20px 0 40px', dispaly: 'flex'}}>
                        <div style={{width: '50%', padding: '0 13px 0 0'}}>
                            <Image src={'/37.309144_55.115305.png'} width={'50%'} height={'50%'} min-height={'50%'}
                                   layout={'responsive'}/>
                            <Link href={'https://maps.google.com?q=55.115305,+37.309144'}><
                                a style={{display: 'block'}}>Посмотреть снимок Google</a>
                            </Link>
                            <Link href={'https://maps.yandex.ru/?ll=37.309144,55.115305&z=17'}>
                                <a style={{display: 'block'}}>Посмотреть снимок Яндекс</a>
                            </Link>
                        </div>
                        <div style={{width: '50%', padding: '0 0 0 13px'}}>
                            <Image src={'/37.3058878_55.1150442.png'} width={'50%'} height={'50%'} min-height={'50%'}
                                   layout={'responsive'}/>
                            <Link href={'https://maps.google.com?q=55.1150442,+37.3058878'}>
                                <a style={{display: 'block'}}>Посмотреть снимок Google</a>
                            </Link>
                            <Link href={'https://maps.yandex.ru/?ll=37.3058878,55.1150442&z=17'}>
                                <a style={{display: 'block'}}>Посмотреть снимок Яндекс</a>
                            </Link>
                        </div>
                    </div>
                    <button style={{fontSize: 20, color: '#006EFF', background: 'transparent', border: 'none'}}
                            onClick={() => setShowHistoryModal(true)}>
                        Посмотреть историю снимков
                        пролета
                    </button>
                    <div style={{textAlign: 'left', margin: '20px 0 0'}}>
                        <div className={style.small} style={{padding: '0 0 16px'}}>
                            Выявленные нарушения
                        </div>
                        <div className={style.params}>
                            ДКР ниже 4 <span>м<span>2</span></span>
                            <strong style={{color: '#006EFF'}}>0</strong>
                        </div>
                        <div className={style.params}>
                            ДКР от 4 до 10 <span>м<span>2</span></span>
                            <strong style={{color: '#EB5757'}}>10</strong>
                        </div>
                        <div className={style.params}>
                            ДКР выше 10 <span>м<span>2</span></span>
                            <strong style={{color: '#EB5757'}}>5</strong>
                        </div>
                        <div className={style.params}>
                            Здания (сооружения)
                            <strong style={{color: '#006EFF'}}>нет</strong>
                        </div>
                    </div>
                    <div style={{textAlign: 'left', margin: '40px 0 0'}}>
                        <div className={style.small}>
                            Длина пролета, м
                            <strong className={style.medium_text}>159.76</strong>
                        </div>
                        <div className={style.small} style={{margin: '20px 0 0'}}>
                            Ширина охранной зоны от оси ВЛ, м:
                        </div>
                        <div className={style.small} style={{
                            margin: '6px 0 0',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'baseline',
                            width: '20%'
                        }}>
                            Слева
                            <strong className={style.medium_text}>23,50</strong>
                        </div>
                        <div className={style.small} style={{
                            margin: '6px 0 0',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'baseline',
                            width: '20%'
                        }}>
                            Справа
                            <strong className={style.medium_text}>23,50</strong>
                        </div>
                        <div className={style.small} style={{
                            margin: '6px 0 0',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'baseline',
                            width: '20%'
                        }}>
                            Общая
                            <strong className={style.medium_text}>47,00</strong>
                        </div>
                        <div className={style.small} style={{margin: '20px 0 0'}}>
                            Площадь насаждений в просеке на длине пролета, <span>м<span>2</span></span> :
                        </div>
                        <div className={style.small} style={{
                            margin: '10px 0 0',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'baseline',
                            width: '20%'
                        }}>
                            Слева
                            <strong className={style.medium_text}>385,18</strong>
                        </div>
                        <div className={style.small} style={{
                            margin: '6px 0 0',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'baseline',
                            width: '20%'
                        }}>
                            Справа
                            <strong className={style.medium_text}>20,68</strong>
                        </div>
                        <div className={style.small} style={{margin: '20px 0 0'}}>
                            Общая площадь, <span>м<span>2</span></span> :
                        </div>
                        <div className={style.small} style={{
                            margin: '10px 0 0',
                        }}>
                            Охранной зоны в пролете ВЛ
                            <strong className={style.medium_text}>6958,81</strong>
                        </div>
                        <div className={style.small} style={{
                            margin: '6px 0 0',
                        }}>
                            Растительности в пролете на ширине просеки
                            <strong className={style.medium_text}>231,88</strong>
                        </div>
                    </div>
                    <div style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItem: 'center',
                        margin: '40px 0 20px'
                    }}>
                        <button className={style.btn}>Добавить в отчет</button>
                        <button className={style.btn} style={{margin: '0 0 0 46px', background: '#006EFF'}}>Сформировать
                            отчет по линии
                        </button>
                    </div>
                </div>
            </ModalModule> : null}
            {isShowHistoryModal ?
                <ModalModule>
                    <div className={style.modal__body}>
                        <div className={style.small} style={{margin: '40px 0 0'}}>
                            Дата <strong className={style.medium_text}>03.12.2021</strong>
                        </div>
                        <div className={style.two_column} style={{margin: '20px 0 40px', dispaly: 'flex'}}>
                            <div style={{width: '50%', padding: '0 13px 0 0'}}>
                                <Image src={'/37.309144_55.115305.png'} width={'50%'} height={'50%'} min-height={'50%'}
                                       layout={'responsive'}/>
                            </div>
                            <div style={{width: '50%', padding: '0 0 0 13px'}}>
                                <Image src={'/37.3058878_55.1150442.png'} width={'50%'} height={'50%'}
                                       min-height={'50%'}
                                       layout={'responsive'}/>
                            </div>
                        </div>
                        <div style={{textAlign: 'left', margin: '20px 0 0'}}>
                            <div className={style.small} style={{padding: '0 0 16px'}}>
                                Выявленные нарушения
                            </div>
                            <div className={style.params}>
                                ДКР ниже 4 <span>м<span>2</span></span>
                                <strong style={{color: '#006EFF'}}>0</strong>
                            </div>
                            <div className={style.params}>
                                ДКР от 4 до 10 <span>м<span>2</span></span>
                                <strong style={{color: '#EB5757'}}>10</strong>
                            </div>
                            <div className={style.params}>
                                ДКР выше 10 <span>м<span>2</span></span>
                                <strong style={{color: '#EB5757'}}>5</strong>
                            </div>
                            <div className={style.params}>
                                Здания (сооружения)
                                <strong style={{color: '#006EFF'}}>нет</strong>
                            </div>
                        </div>
                        <div className={style.small} style={{margin: '40px 0 0'}}>
                            Дата <strong className={style.medium_text}>01.12.2021</strong>
                        </div>
                        <div className={style.two_column} style={{margin: '20px 0 40px', dispaly: 'flex'}}>
                            <div style={{width: '50%', padding: '0 13px 0 0'}}>
                                <Image src={'/37.309144_55.115305.png'} width={'50%'} height={'50%'} min-height={'50%'}
                                       layout={'responsive'}/>
                            </div>
                            <div style={{width: '50%', padding: '0 0 0 13px'}}>
                                <Image src={'/37.3058878_55.1150442.png'} width={'50%'} height={'50%'}
                                       min-height={'50%'}
                                       layout={'responsive'}/>
                            </div>
                        </div>
                        <div style={{textAlign: 'left', margin: '20px 0 0'}}>
                            <div className={style.small} style={{padding: '0 0 16px'}}>
                                Выявленные нарушения
                            </div>
                            <div className={style.params}>
                                ДКР ниже 4 <span>м<span>2</span></span>
                                <strong style={{color: '#006EFF'}}>0</strong>
                            </div>
                            <div className={style.params}>
                                ДКР от 4 до 10 <span>м<span>2</span></span>
                                <strong style={{color: '#EB5757'}}>10</strong>
                            </div>
                            <div className={style.params}>
                                ДКР выше 10 <span>м<span>2</span></span>
                                <strong style={{color: '#EB5757'}}>5</strong>
                            </div>
                            <div className={style.params}>
                                Здания (сооружения)
                                <strong style={{color: '#006EFF'}}>нет</strong>
                            </div>
                        </div>
                        <div className={style.small} style={{margin: '40px 0 0'}}>
                            Дата <strong className={style.medium_text}>11.11.2021</strong>
                        </div>
                        <div className={style.two_column} style={{margin: '20px 0 40px', dispaly: 'flex'}}>
                            <div style={{width: '50%', padding: '0 13px 0 0'}}>
                                <Image src={'/37.309144_55.115305.png'} width={'50%'} height={'50%'} min-height={'50%'}
                                       layout={'responsive'}/>
                            </div>
                            <div style={{width: '50%', padding: '0 0 0 13px'}}>
                                <Image src={'/37.3058878_55.1150442.png'} width={'50%'} height={'50%'}
                                       min-height={'50%'}
                                       layout={'responsive'}/>
                            </div>
                        </div>
                        <div style={{textAlign: 'left', margin: '20px 0 0'}}>
                            <div className={style.small} style={{padding: '0 0 16px'}}>
                                Выявленные нарушения
                            </div>
                            <div className={style.params}>
                                ДКР ниже 4 <span>м<span>2</span></span>
                                <strong style={{color: '#006EFF'}}>0</strong>
                            </div>
                            <div className={style.params}>
                                ДКР от 4 до 10 <span>м<span>2</span></span>
                                <strong style={{color: '#EB5757'}}>10</strong>
                            </div>
                            <div className={style.params}>
                                ДКР выше 10 <span>м<span>2</span></span>
                                <strong style={{color: '#EB5757'}}>5</strong>
                            </div>
                            <div className={style.params}>
                                Здания (сооружения)
                                <strong style={{color: '#006EFF'}}>нет</strong>
                            </div>
                        </div>
                    </div>
                </ModalModule>
                : null
            }
        </Polygon>
    )
}

const redIcon = L.icon({iconUrl: "/red.svg"});
const blueIcon = L.icon({iconUrl: "/blue.svg"});

export default function MapModule({geo}) {
    const [position, setPosition] = useState([60.00000, 100.000000])

    return (
        <div className={mapStyle.map}>
            <MapContainer center={position} zoom={4} scrollWheelZoom={false}
                          style={{height: "100%", width: "100%", borderRadius: "24px"}}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {Object.entries(geo).map(([keys, {id, h3, val, color}], i) => {
                    return <Hexagon key={id} id={id} h3={h3} color={color}/>
                })}
            </MapContainer>

        </div>
    )
}



